package com.cg.webdriver.webdriver_java_training;

import java.util.HashSet;
import java.util.Set;

import com.github.javafaker.Faker;

public class RandomValues {
	// class variable
	final String lexicon = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
	final String lexiconZip = "1234567890";
			
	final java.util.Random rand = new java.util.Random();

	// consider using a Map<String,Boolean> to say whether the identifier is being
	// used or not
	final Set<String> identifiers = new HashSet<String>();

	public String generateRandomName(int length) {

		StringBuilder builder = new StringBuilder();
		while (builder.toString().length() == 0) {
			for (int i = 0; i < length; i++) {
				builder.append(lexicon.charAt(rand.nextInt(lexicon.length())));
			}
			if (identifiers.contains(builder.toString())) {
				builder = new StringBuilder();
			}
		}
		return builder.toString();
	}
	
	public String generateRandomZipCode(int length) {

		StringBuilder builder = new StringBuilder();
		while (builder.toString().length() == 0) {
			for (int i = 0; i < length; i++) {
				builder.append(lexiconZip.charAt(rand.nextInt(lexiconZip.length())));
			}
			if (identifiers.contains(builder.toString())) {
				builder = new StringBuilder();
			}
		}
		return builder.toString();
	}
	
	public static void main(String[] args) {
		RandomValues r = new RandomValues();
		System.out.println(r.generateRandomName(6));
		System.out.println(r.generateRandomZipCode(6));
		
		Faker faker = new Faker();
		System.out.println(faker.name().name());
		System.out.println(faker.address().streetAddress());
		System.out.println(faker.address().city());
		System.out.println(faker.address().state());
	}
}
