package com.cg.webdriver.webdriver_java_training.day2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import junit.framework.Assert;

public class InitiatingWebDriver {
	
	public static void main(String args[]) {
		System.setProperty("webdriver.chrome.driver", "C:\\DEV\\Softwares\\Drivers\\chromedriver-win64\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.demo.guru99.com/v4/index.php");
		driver.findElement(By.cssSelector("body > form > table > tbody > tr:nth-child(1) > td:nth-child(2) > input[type=text]")).sendKeys("testUser");
		driver.findElement(By.xpath("//td[text()='Password']/following::input[1]")).sendKeys("testPassword");
		
		String title = driver.getTitle();
		System.out.println(title);
		
		if("Guru99 Bank Home Page1".equals(title)) {
			System.out.println("Page Title is as expected");
		}
		else {
			System.out.println("expected title is Guru99 Bank Home Page1 but was "+ title);
		}
		
		Assert.assertEquals("Guru99 Bank Home Page", title);
		
		driver.findElement(By.name("btnReset")).click();
		driver.close();
		driver.quit();
		System.exit(0);
		
	}
}
