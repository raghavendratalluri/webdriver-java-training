package com.cg.webdriver.webdriver_java_training.day3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class IntiatingWebDriver {
	public static void main(String args[]) throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\DEV\\Softwares\\Drivers\\chromedriver-win64\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
//		driver.get("https://www.demo.guru99.com/v4/index.php");
		driver.get("https://docs.oracle.com/javase/8/docs/api/index.html?java/lang/String.html");
//		driver.findElement(
//				By.cssSelector("body > form > table > tbody > tr:nth-child(1) > td:nth-child(2) > input[type=text]"))
//				.sendKeys("testUser");
//		driver.findElement(By.xpath("//td[text()='Password']/following::input[1]")).sendKeys("testPassword");
//
//		String title = driver.getTitle();
//		System.out.println(title);
//
//		if ("Guru99 Bank Home Page1".equals(title)) {
//			System.out.println("Page Title is as expected");
//		} else {
//			System.out.println("expected title is Guru99 Bank Home Page1 but was " + title);
//		}
//
//		Assert.assertEquals("Guru99 Bank Home Page", title);
//
//		driver.findElement(By.name("btnReset")).click();
		
//		List<WebElement> allLinks = driver.findElements(By.tagName("input"));
//		
//		for (WebElement webElement : allLinks) {
//			System.out.println(webElement.getAttribute("name"));
//		}
		
//		System.out.println(driver.getPageSource());
//		System.out.println(driver.getCurrentUrl());
		
//		Thread.sleep(5000);
//		driver.navigate().refresh();
//		Thread.sleep(5000);
//		driver.findElement(By.linkText("here")).click();
//		Thread.sleep(5000);
//		driver.navigate().back();
//		Thread.sleep(5000);
//		driver.navigate().forward();
		
//		WebElement packagesFrame = driver.findElement(By.xpath("//frame[@title='All Packages']"));
//		
//		driver.switchTo().frame(packagesFrame);
//		
//		driver.findElement(By.linkText("java.lang")).click();
		
		
		
		
		driver.close();
		driver.quit();
		System.exit(0);
	}
}
