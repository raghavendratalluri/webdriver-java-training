package com.cg.webdriver.webdriver_java_training.quiz;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Quiz3 {
	
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\DEV\\Softwares\\Drivers\\chromedriver-win64\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		// What is the output of this
		driver.get("http://www.google.com");
		
		List<WebElement> wes =  driver.findElements(By.className("gLFyf"));
		System.out.println(wes);
		System.out.println(wes.size());
		if(null!=wes && wes.size() > 0)
		{
			wes.get(0).sendKeys("What is Selenium WebDriver" + Keys.ENTER);
			Thread.sleep(3000);
		}
		
		System.out.println("hello");
		
		
		
		driver.close();
		driver.quit();
		System.exit(0);
	}

}
