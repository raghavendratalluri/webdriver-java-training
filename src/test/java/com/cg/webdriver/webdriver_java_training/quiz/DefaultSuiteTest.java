package com.cg.webdriver.webdriver_java_training.quiz;


import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.net.MalformedURLException;
import java.net.URL;
public class DefaultSuiteTest {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  Actions a;
  @Before
  public void setUp() {
	  System.setProperty("webdriver.chrome.driver",
				"C:\\DEV\\Softwares\\Drivers\\chromedriver-win64\\chromedriver.exe");
    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
    a = new Actions(driver);
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  @Test
  public void test1() throws InterruptedException {
    driver.get("https://www.redbus.com/");
    driver.manage().window().maximize();
    driver.findElement(By.linkText("Chennai to Tirupathi Bus")).click();
    driver.findElement(By.xpath("//a[@id='searchBusLink'][1]")).click();
    driver.findElement(By.xpath("//span[contains(text(),'View Seats')]")).click();
    driver.findElement(By.xpath("//i[@class='icon-close closepopupbtn']")).click();

    WebElement canvas = driver.findElement(By.xpath("//canvas[@data-type='lower']"));
//    js.executeScript("window.scrollTo(0,594)");
    
    for (int i = 0; i < 386; i=i+30) {
		a.moveToElement(canvas, 30, i).perform();
	}
//    driver.findElement(By.cssSelector(".db:nth-child(1) > div > .radio-css > .radio-unchecked")).click();
  }
}
