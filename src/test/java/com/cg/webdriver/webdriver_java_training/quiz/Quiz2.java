package com.cg.webdriver.webdriver_java_training.quiz;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Quiz2 {
	
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"C:\\DEV\\Softwares\\Drivers\\chromedriver-win64\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		// What is the output of this
		driver.get("http://www.google.com");
		
		// What is the return type of this below code
		System.out.println(driver.getWindowHandles());
		
		List<WebElement> links = driver.findElements(By.tagName("a"));
		
		for (WebElement webElement : links) {
			System.out.println(webElement.getText());
		}

		
	}

}
