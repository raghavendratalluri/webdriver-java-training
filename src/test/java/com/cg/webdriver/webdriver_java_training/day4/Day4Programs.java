package com.cg.webdriver.webdriver_java_training.day4;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Day4Programs {
	public static void main(String args[]) throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\DEV\\Softwares\\Drivers\\chromedriver-win64\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
		
//		driver.get("https://www.demo.guru99.com/v4/index.php");
//		driver.get("https://demo.guru99.com/test/drag_drop.html");
		driver.get("https://demo.guru99.com/popup.php");
//		driver.get("https://docs.oracle.com/javase/8/docs/api/index.html?java/lang/String.html");
		
//		driver.get("https://www.selenium.dev/selenium/web/alerts.html#");

//		driver.findElement(By.id("alert")).click();
//		Thread.sleep(4000);
//		System.out.println(driver.switchTo().alert().getText());
//		Thread.sleep(4000);
//		driver.switchTo().alert().accept();
		
//		driver.findElement(By.id("empty-alert")).click();
//		Thread.sleep(4000);
//		System.out.println("******"+ driver.switchTo().alert().getText() +"******");
//		Thread.sleep(4000);
//		driver.switchTo().alert().accept();
		
//		driver.findElement(By.id("prompt")).click();
//		Thread.sleep(4000);
//		driver.switchTo().alert().dismiss();
//		Thread.sleep(4000);
//		WebDriverWait explicitWait = new WebDriverWait(driver, Duration.ofSeconds(30));
//		driver.findElement(By.id("prompt")).click();
//		explicitWait.until(ExpectedConditions.alertIsPresent());
//		driver.switchTo().alert().sendKeys("Sample Alert");
//		driver.switchTo().alert().accept();
		
//		try {
//			driver.findElement(By.name("uidkugsaugs")).sendKeys("test");
//		}
//		catch(NoSuchElementException e) {
//			System.out.println(e.getMessage());
//			driver.findElement(By.name("uid")).sendKeys("test");
//			Thread.sleep(4000);
//		}
//		catch(ArrayIndexOutOfBoundsException ae) {
//			
//		}
//		catch(Exception e) {
//			
//		}
		
//      driver.findElement(By.name("uid")).sendKeys("mngr571878");
//      
//      driver.findElement(By.name("password")).sendKeys("UmUpUqy");
//      
//      driver.findElement(By.name("btnLogin")).click();
//      
//      driver.findElement(By.linkText("New Customer")).click();
//      
//      List<WebElement> allFrames = driver.findElements(By.tagName("iframe"));
      
//      WebDriverWait exWait = new WebDriverWait(driver, Duration.ofSeconds(30));
//      exWait.until(ExpectedConditions.visibilityOfAllElements(allFrames));
      
      
      
//      if(allFrames.size() > 0) {
//    	  
//    	  for (WebElement iframe : allFrames) {
//    		  driver.switchTo().frame(iframe);
//    		  List<WebElement> innerFrames = driver.findElements(By.tagName("iframe"));
//    		  if(innerFrames.size() > 0) {
//    			  for (WebElement innerFrame : innerFrames) {
//    				  driver.switchTo().frame(innerFrame);
//    				  List<WebElement> closeButton = driver.findElements(By.xpath("//*[text()='Close']"));
//    	    		  if(closeButton.size() > 0) {
//    	    			  driver.findElement(By.xpath("//*[text()='Close']")).click();
//    	    			  driver.switchTo().defaultContent();
//    	    			  break;
//    	    		  }
//    	    		  driver.switchTo().defaultContent();
//    	    		  driver.switchTo().frame(iframe);
//				}
//    		  }
//    		  
//    		  driver.switchTo().defaultContent();
//    		  
//		}
//    	  
//      }
		
//      JavascriptExecutor js = (JavascriptExecutor) driver;
//      
//      driver.findElement(By.name("name")).sendKeys("David Smith");
//      driver.findElement(By.xpath("//*[contains(text(),'Gender')]/following::input[1]")).click();
//      driver.findElement(By.id("dob")).sendKeys("09/13/1997");
////      js.executeScript("document.getElementById('dob').value='09/13/1997'");
//      driver.findElement(By.name("addr")).sendKeys("12 Balaji temple Rd");
//      driver.findElement(By.name("city")).sendKeys("Bengaluru");
//      driver.findElement(By.name("state")).sendKeys("Karnataka");
//      driver.findElement(By.name("pinno")).sendKeys("573098");
//      driver.findElement(By.name("telephoneno")).sendKeys("9867544321");
//      driver.findElement(By.name("emailid")).sendKeys("davidsmii1uf5ith@xyz2.com");
//      driver.findElement(By.name("password")).sendKeys("TestPassword");
//      driver.findElement(By.name("sub")).click();
//      String dynamicTableXpath = "//*[text()='labelValue']/following::td[1]";
//      System.out.println(driver.findElement(By.xpath(dynamicTableXpath.replaceFirst("labelValue", "Customer ID"))).getText());
//      System.out.println(driver.findElement(By.xpath(dynamicTableXpath.replaceFirst("labelValue", "Customer Name"))).getText());
		
      
//		WebElement src = driver.findElement(By.xpath("//*[@id='credit2']/a"));
//		WebElement tar = driver.findElement(By.xpath("//*[@id='bank']"));
//		
//      Actions a = new Actions(driver);
//      a.dragAndDrop(src, tar).perform();
		
		System.out.println(driver.getWindowHandle());
		System.out.println(driver.getWindowHandles());
		
		Set<String> before = driver.getWindowHandles();
		
		driver.findElement(By.linkText("Click Here")).click();
		
		System.out.println("After");
		System.out.println(driver.getWindowHandle());
		System.out.println(driver.getWindowHandles());
		
		Set<String> after = driver.getWindowHandles();
		
		after.removeAll(before);
		
		String secondWindow = after.iterator().next();
		
		driver.switchTo().window(secondWindow);
		
		
		driver.findElement(By.name("btnLogin")).click();
      
      Thread.sleep(6000);
      
		
		driver.close();
		driver.quit();
		System.exit(0);
	}
}
