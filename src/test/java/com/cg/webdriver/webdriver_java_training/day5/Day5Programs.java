package com.cg.webdriver.webdriver_java_training.day5;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Day5Programs {
	public static void main(String args[]) throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\DEV\\Softwares\\Drivers\\chromedriver-win64\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.get("https://www.demo.guru99.com/v4/index.php");
		
      driver.findElement(By.name("uid")).sendKeys(System.getProperty("userName"));
      
      driver.findElement(By.name("password")).sendKeys(System.getProperty("password"));
      
      driver.findElement(By.name("btnLogin")).click();
      
      driver.findElement(By.linkText("New Customer")).click();
//      
//      List<WebElement> allFrames = driver.findElements(By.tagName("iframe"));
      
//      WebDriverWait exWait = new WebDriverWait(driver, Duration.ofSeconds(30));
//      exWait.until(ExpectedConditions.visibilityOfAllElements(allFrames));
      
      
      
//      if(allFrames.size() > 0) {
//    	  
//    	  for (WebElement iframe : allFrames) {
//    		  driver.switchTo().frame(iframe);
//    		  List<WebElement> innerFrames = driver.findElements(By.tagName("iframe"));
//    		  if(innerFrames.size() > 0) {
//    			  for (WebElement innerFrame : innerFrames) {
//    				  driver.switchTo().frame(innerFrame);
//    				  List<WebElement> closeButton = driver.findElements(By.xpath("//*[text()='Close']"));
//    	    		  if(closeButton.size() > 0) {
//    	    			  driver.findElement(By.xpath("//*[text()='Close']")).click();
//    	    			  driver.switchTo().defaultContent();
//    	    			  break;
//    	    		  }
//    	    		  driver.switchTo().defaultContent();
//    	    		  driver.switchTo().frame(iframe);
//				}
//    		  }
//    		  
//    		  driver.switchTo().defaultContent();
//    		  
//		}
//    	  
//      }
		
//      JavascriptExecutor js = (JavascriptExecutor) driver;
//      
      driver.findElement(By.name("name")).sendKeys("David Adam");
      driver.findElement(By.xpath("//*[contains(text(),'Gender')]/following::input[1]")).click();
      driver.findElement(By.id("dob")).sendKeys("08/13/1997");
//      js.executeScript("document.getElementById('dob').value='09/13/1997'");
      driver.findElement(By.name("addr")).sendKeys("1245 Balaji temple Rd");
      driver.findElement(By.name("city")).sendKeys("Chennai");
      driver.findElement(By.name("state")).sendKeys("Tamilnadu");
      driver.findElement(By.name("pinno")).sendKeys("593098");
      driver.findElement(By.name("telephoneno")).sendKeys("9867545321");
      driver.findElement(By.name("emailid")).sendKeys("dav1uf5ith@xyz5.com");
      driver.findElement(By.name("password")).sendKeys("TestPassword");
      driver.findElement(By.name("sub")).click();
      Thread.sleep(5000);
      String dynamicTableXpath = "//*[text()='labelValue']/following::td[1]";
      System.out.println(driver.findElement(By.xpath(dynamicTableXpath.replaceFirst("labelValue", "Customer ID"))).getText());
      System.out.println(driver.findElement(By.xpath(dynamicTableXpath.replaceFirst("labelValue", "Customer Name"))).getText());
		
      List<WebElement> alltrs = driver.findElements(By.xpath("//table[@id='customer']/tbody/tr"));
      System.out.println(alltrs.size());
      for (WebElement webElement : alltrs) {
		System.out.println(webElement.getText());
	}
		
		
		driver.close();
		driver.quit();
		System.exit(0);
	}
}
