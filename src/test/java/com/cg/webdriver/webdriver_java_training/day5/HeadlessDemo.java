package com.cg.webdriver.webdriver_java_training.day5;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.cg.webdriver.webdriver_java_training.RandomValues;
import com.github.javafaker.Faker;

public class HeadlessDemo {
	public static void main(String[] args) throws InterruptedException {

		Faker faker = new Faker();

		RandomValues r = new RandomValues();
		System.setProperty("webdriver.chrome.driver",
				"C:\\DEV\\Softwares\\Drivers\\chromedriver-win64\\chromedriver.exe");
		ChromeOptions co = new ChromeOptions();
		co.addArguments("--headless");
		WebDriver driver = new ChromeDriver(co);
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("https://www.demo.guru99.com/v4/index.php");

		driver.findElement(By.name("uid")).sendKeys(System.getProperty("userName"));

		driver.findElement(By.name("password")).sendKeys(System.getProperty("password"));

		driver.findElement(By.name("btnLogin")).click();

		driver.findElement(By.linkText("New Customer")).click();
//  
//  List<WebElement> allFrames = driver.findElements(By.tagName("iframe"));

//  WebDriverWait exWait = new WebDriverWait(driver, Duration.ofSeconds(30));
//  exWait.until(ExpectedConditions.visibilityOfAllElements(allFrames));

//  if(allFrames.size() > 0) {
//	  
//	  for (WebElement iframe : allFrames) {
//		  driver.switchTo().frame(iframe);
//		  List<WebElement> innerFrames = driver.findElements(By.tagName("iframe"));
//		  if(innerFrames.size() > 0) {
//			  for (WebElement innerFrame : innerFrames) {
//				  driver.switchTo().frame(innerFrame);
//				  List<WebElement> closeButton = driver.findElements(By.xpath("//*[text()='Close']"));
//	    		  if(closeButton.size() > 0) {
//	    			  driver.findElement(By.xpath("//*[text()='Close']")).click();
//	    			  driver.switchTo().defaultContent();
//	    			  break;
//	    		  }
//	    		  driver.switchTo().defaultContent();
//	    		  driver.switchTo().frame(iframe);
//			}
//		  }
//		  
//		  driver.switchTo().defaultContent();
//		  
//	}
//	  
//  }

//  JavascriptExecutor js = (JavascriptExecutor) driver;
//  
		String name = faker.name().firstName() + " " + faker.name().lastName();
		System.out.println(name);
		driver.findElement(By.name("name")).sendKeys(name);
		driver.findElement(By.xpath("//*[contains(text(),'Gender')]/following::input[1]")).click();
		driver.findElement(By.id("dob")).sendKeys("03/13/1996");
//  js.executeScript("document.getElementById('dob').value='09/13/1997'");
		driver.findElement(By.name("addr")).sendKeys(faker.address().streetAddress());
		driver.findElement(By.name("city")).sendKeys(faker.address().city());
		driver.findElement(By.name("state")).sendKeys(faker.address().state());
		driver.findElement(By.name("pinno")).sendKeys(r.generateRandomZipCode(6));
		driver.findElement(By.name("telephoneno")).sendKeys(r.generateRandomZipCode(10));
		String email = name.replaceAll(" ", "") + r.generateRandomZipCode(10);
		email = email.substring(0, 21) + "@xyz.com";
		System.out.println(email);
		driver.findElement(By.name("emailid")).sendKeys(email);
		driver.findElement(By.name("password")).sendKeys("TestPassword");
		driver.findElement(By.name("sub")).click();
//		Thread.sleep(5000);
		String dynamicTableXpath = "//*[text()='labelValue']/following::td[1]";
		System.out.println(
				driver.findElement(By.xpath(dynamicTableXpath.replaceFirst("labelValue", "Customer ID"))).getText());
		System.out.println(
				driver.findElement(By.xpath(dynamicTableXpath.replaceFirst("labelValue", "Customer Name"))).getText());

		List<WebElement> alltrs = driver.findElements(By.xpath("//table[@id='customer']/tbody/tr"));
		System.out.println(alltrs.size());
		for (WebElement webElement : alltrs) {
			System.out.println(webElement.getText());
		}

		driver.close();
		driver.quit();
		System.exit(0);
	}
}
