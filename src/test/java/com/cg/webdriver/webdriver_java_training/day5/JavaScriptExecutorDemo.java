package com.cg.webdriver.webdriver_java_training.day5;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class JavaScriptExecutorDemo {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\DEV\\Softwares\\Drivers\\chromedriver-win64\\chromedriver.exe");
        WebDriver driver= new ChromeDriver();			
        		
        //Creating the JavascriptExecutor interface object by Type casting		
        JavascriptExecutor js = (JavascriptExecutor)driver;		
        		
        //Launching the Site.		
        driver.get("https://demo.guru99.com/V4/");			
        		
        		
        //Login to Guru99 	
        js.executeScript("arguments[0].value='mngr571878';", driver.findElement(By.name("uid")));
        js.executeScript("arguments[0].value='UmUpUqy';", driver.findElement(By.name("password")));
        		
        //Perform Click on LOGIN button using JavascriptExecutor	
        WebElement button =driver.findElement(By.name("btnLogin"));	
        js.executeScript("arguments[0].click();", button);
                                
        //To generate Alert window using JavascriptExecutor. Display the alert message 			
        js.executeScript("alert('Welcome to Guru99');"); 
        
        Thread.sleep(7000);
        WebDriverWait explicitWait = new WebDriverWait(driver, Duration.ofSeconds(10));
        explicitWait.until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert().accept();
        
        driver.close();
        driver.quit();
        System.exit(0);
	}
}
