package com.cg.webdriver.webdriver_java_training.day5;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class CanvasDemo {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\DEV\\Softwares\\Drivers\\chromedriver-win64\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("http://www.htmlcanvasstudio.com/painterpage.aspx");

		driver.findElement(By.xpath("//input[@title='Draw a line']")).click();

		Actions a = new Actions(driver);

		WebElement canvasArea = driver.findElement(By.id("imageTemp"));

		a.moveToElement(canvasArea, -100, -100).clickAndHold().moveByOffset(200, 200).click().build().perform();

		Thread.sleep(5000);
		driver.close();
		driver.quit();
		System.exit(0);
	}

}
