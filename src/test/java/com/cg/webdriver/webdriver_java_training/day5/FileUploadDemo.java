package com.cg.webdriver.webdriver_java_training.day5;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class FileUploadDemo {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"C:\\DEV\\Softwares\\Drivers\\chromedriver-win64\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("https://demo.guru99.com/test/upload/");

		driver.findElement(By.name("uploadfile_0")).sendKeys("C:\\DEV\\Softwares\\Images\\Untitled.png");
		
		driver.findElement(By.id("terms")).click();
		
		driver.findElement(By.id("submitbutton")).click();
		
		System.out.println(driver.findElement(By.xpath("//h3[@id='res']/center")).getText());

		driver.close();
		driver.quit();
		System.exit(0);
	}

}
